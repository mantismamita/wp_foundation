<?php

function wp_foundation_scripts() {
	wp_enqueue_script( 'bower-modernizr', get_template_directory_uri() . '/bower_components/modernizr/modernizr.min.js', array(), '2.8.3', false );
	
	wp_enqueue_script( 'bower-foundation', get_template_directory_uri() . '/bower_components/foundation/js/foundation.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.js', array('jquery'), '1.0.0' , true );
			
	wp_enqueue_script('google-maps', '//maps.google.com/maps/api/js?key=AIzaSyCSAVx-HQFqCCFvzQ_WIJCDLZL2uiOIvSA&sensor=true', array(), '3', true);
			

}

add_action( 'wp_enqueue_scripts', 'wp_foundation_scripts' );
