<?php
/**
 * The Template for displaying all single posts.
 *
 * @package wp_foundation
 */

get_header(); ?>

<div id="mini-featured-image" role="img" aria-label="Caulaincourt Boutique Hostel Bar">
<div class="medallion-anchor">
<?php $charter = lang_post_id(2123);?>	
	<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
	
    <a href="index.php?page_id=<?php echo $charter . '&lang=' . ICL_LANGUAGE_CODE ?>">
     <image xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/medallion-en.png" src="<?php echo get_stylesheet_directory_uri(); ?>/img/medallion-en.svg" class="medallion hide-for-small-only" alt="medallion" width="150" height="150" /></a>
     
<?php }	 elseif (ICL_LANGUAGE_CODE == 'fr') {?>   
     <a href="index.php?page_id=<?php echo $charter . '&lang=' . ICL_LANGUAGE_CODE ?>">
     <image xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/medallion-fr.png" src="<?php echo get_stylesheet_directory_uri(); ?>/img/medallion-fr.svg" class="medallion hide-for-small-only" alt="medallion" width="150" height="150" /></a>
     
   <?php }	?> 
</div>
<p class="reservelink text-right"><a href="#" class="small primary button radius show-for-small" data-reveal-id="resModal"><?php _e('Reserve', 'wp_foundation' )?></a></p> 
</div>
		
	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
    <div class="pagetitle">         
  <div class="row">
    <div class="large-12 columns text-center"> 
    <h1 class="entry-title"><?php the_title(); ?></h1>
    </div><!--  large 12 -->
    </div> <!-- end row -->
	</div> <!-- end pagetitle -->
	</header><!-- .entry-header -->
	<section class="main">
	<div class="row">
	<div id="primary" class="content-area large-9 columns">
		<main id="main" class="site-main" role="main">
    
		<?php while ( have_posts() ) : the_post(); ?>
		<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs">
		<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
		</div>
		<?php } ?>
			<div class="trans-panel">

			<?php get_template_part( 'content', 'single' ); ?>
			</div>
			<?php wp_foundation_content_nav( 'nav-below' ); ?>

			<!--
<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>
-->

		<?php endwhile; // end of the loop. ?>
		</main>
		</div><!-- #primary -->
	

<?php get_sidebar(); ?>

</article><!-- #post-## -->	
</div> <!-- end row -->
 </section>
<?php get_footer(); ?>