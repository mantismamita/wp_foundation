<?php
/**
 * @package wp_foundation
 */
?>

	
		
		<?php
        if ( has_post_thumbnail() ) { ?>
                
        <div class="post-feature-image">        
		<?php the_post_thumbnail();
echo '<p class="wp-caption-text hover-caption">' . get_post(get_post_thumbnail_id())->post_excerpt . '</p>'; ?>
        </div>		
         <?php } ?>
	<div class="entry-content">
		
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wp_foundation' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
	<?php wp_foundation_posted_on(); ?>
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'wp_foundation' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'wp_foundation' ) );

			if ( ! wp_foundation_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( '<a href="%3$s" rel="bookmark" class="tiny secondary button alignright">Bookmark this page</a>', 'wp_foundation' );
				} else {
					$meta_text = __( '<a href="%3$s" rel="bookmark" class="tiny secondary button alignright">Bookmark this page</a>', 'wp_foundation' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'in %1$s and tagged %2$s. <a href="%3$s" rel="bookmark" class="tiny secondary button alignright">Bookmark this page</a>', 'wp_foundation' );
				} else {
					$meta_text = __( 'in %1$s. <a href="%3$s" rel="bookmark" class="tiny secondary button alignright">Bookmark this page</a>', 'wp_foundation' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>
<button class="tiny secondary button alignright ">
		<?php edit_post_link( __( 'Edit', 'wp_foundation' ), '<span class="edit-link">', '</span>' ); ?>
</button>
	</footer><!-- .entry-meta -->

