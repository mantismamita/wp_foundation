<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package wp_foundation
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

	
    <header class="entry-header">
    <div class="pagetitle">         
		<div class="row">
	    <div class="large-12 columns text-center"> 
	    <h1 class="page-title headline ondark"><?php _e( 'Page not found', 'wp_foundation' ); ?></h1>
	    </div>
	    </div>
	</div> 
	</header><!-- .entry-header -->

	<section class="error-404 not-found main">
		
			
			<div class="large-9 large-centered columns">
					<h2><?php _e( 'Hmm...', 'wp_foundation' ); ?></h1>
					<h4><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'wp_foundation' ); ?></h4>

					<?php get_search_form(); ?>

					
                    <h4><?php _e( 'or one of these links?', 'wp_foundation' ); ?></h4>
                    <ul class="no-bullet">
                    <?php $args = array(
						'depth'        => 0,
						'show_date'    => '',
						'date_format'  => get_option('date_format'),
						'child_of'     => 0,
						'exclude'      => 2,
						'include'      => '',
						'title_li'     => '',
						'echo'         => 1,
						'authors'      => '',
						'sort_column'  => 'menu_order',
						'link_before'  => '',
						'link_after'   => '',
						'walker'       => '',
						'post_type'    => 'page',
					    'post_status'  => 'publish' 
					); ?>

                    <?php wp_list_pages( $args ); ?>
                    </ul>
				</div>	
		
				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>