<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package wp_foundation
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header fancy">
    <div class="pagetitle">         
  <div class="row">
    <div class="large-12 columns text-center"> 
    <h1 class="entry-title"><?php the_title(); ?></h1>
    </div>
    </div>
</div> 
	</header><!-- .entry-header -->
<section class="main">
		<div class="row">

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wp_foundation' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</div>	
</section>
</article><!-- #post-## -->
