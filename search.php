<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package wp_foundation
 */

get_header(); ?>

<div id="mini-featured-image" role="img" aria-label="Caulaincourt Boutique Hostel Bar"></div>
<div id="ie8res">
	<?php get_template_part( 'content-basicres', get_post_format() ); ?>
</div>

<header class="entry-header">
    <div class="pagetitle">         
  <div class="row">
    <div class="large-12 columns text-center"> 
    
    <h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'wp_foundation' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    </div><!--  large 12 -->
    </div> <!-- end row -->
	</div> <!-- end pagetitle -->
	</header><!-- .entry-header -->
<section class="main">	
  <div class="row">
    <div class="large-9 columns">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
		<?php if ( have_posts() ) : ?>
		<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php wp_foundation_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'search' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
    </div><!--  end large 9 -->

<?php get_sidebar(); ?>
  </div>
</section>
<?php get_template_part( 'content-quickres', get_post_format() ); ?>
<?php get_footer(); ?>