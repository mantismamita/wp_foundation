<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package wp_foundation
 */
?>
	<div id="secondary" class="widget-area large-3 light-panel columns" role="complementary">
	

			
<aside id="meta" class="widget">
				<h1 class="widget-title"><?php _e( 'Meta', 'wp_foundation' ); ?></h1>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>


		

	</div><!-- #secondary -->
