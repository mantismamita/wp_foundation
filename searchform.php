<?php
/**
 * The template for displaying search forms in wp_foundation
 *
 * @package wp_foundation
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php _ex( 'Search for:', 'label', 'wp_foundation' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'wp_foundation' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
	</label>
	<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Search', 'submit button', 'wp_foundation' ); ?>">
</form>
