<?php
/**
 * wp_foundation functions and definitions
 *
 * @package wp_foundation
 */
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT.'/img');


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'wp_foundation_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function wp_foundation_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on wp_foundation, use a find and replace
	 * to change 'wp_foundation' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wp_foundation', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wp_foundation' ),
		'secondary' => __( 'Secondary Menu', 'wp_foundation' ),
	) );
	
	

//adds modal link to nav menu item "Reserve"
add_filter( 'nav_menu_link_attributes', 'wp_foundation_menu_atts', 10, 3 );
function wp_foundation_menu_atts( $atts, $item, $args )
{
  // The ID of the target menu item
  
  $menu_target_fr = 1790;
  $menu_target_en = 1786;

  // inspect $item
  if (($item->ID == $menu_target_fr) || ($item->ID == $menu_target_en)) {
    $atts['data-reveal-id'] = 'resModal';
  }
  return $atts;
}
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	add_theme_support( 'custom-background', apply_filters( 'wp_foundation_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // wp_foundation_setup
add_action( 'after_setup_theme', 'wp_foundation_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
/**
 * Register widgetized area and update sidebar with default widgets
 */
function wp_foundation_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wp_foundation' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar(array(
			'name' => __('First Footer Widget Area', 'wp_foundation'),
			'id' => 'first-footer-widget-area',
			'description' => __('The first footer widget area', 'wp_foundation'),
			'before_widget' => '<div id="%1$s" class="small_list widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));

	// Area 4, located in the footer. Empty by default.
	register_sidebar(array(
			'name' => __('Second Footer Widget Area', 'wp_foundation'),
			'id' => 'second-footer-widget-area',
			'description' => __('The second footer widget area', 'wp_foundation'),
			'before_widget' => '<div id="%1$s" class="small_list widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));

	// Area 5, located in the footer. Empty by default.
	register_sidebar(array(
			'name' => __('Third Footer Widget Area', 'wp_foundation'),
			'id' => 'third-footer-widget-area',
			'description' => __('The third footer widget area', 'wp_foundation'),
			'before_widget' => '<div id="%1$s" class="small_list widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));

	// Area 6, located in the footer. Empty by default.
	register_sidebar(array(
			'name' => __('Fourth Footer Widget Area', 'wp_foundation'),
			'id' => 'fourth-footer-widget-area',
			'description' => __('The fourth footer widget area', 'wp_foundation'),
			'before_widget' => '<div id="%1$s" class="small_list widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));
}

add_action( 'widgets_init', 'wp_foundation_widgets_init' );

/**
 * Enqueue scripts and styles
 */
	
function test() {
	
	wp_enqueue_script( 'wp_foundation-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	/*
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
*/

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'wp_foundation-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202', true );
	}
}
add_action( 'wp_enqueue_scripts', 'test' );

/**
 * Enqueue Scripts and Styles for Front-End
 */
 
// Enqueue scripts
require_once('inc/enqueue-scripts.php');

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';

//allows svg in media uploader
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function isEmail($verify_email) {
	
		return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$verify_email));
	
	}
	
function wp_foundation_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('new-content'); // This removes the complete menu “Add New”. You will not require the below “remove_menu” if you using this line.
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('search');
}

add_action('wp_before_admin_bar_render', 'wp_foundation_admin_bar_remove', 0);


function wp_foundation_login_logo() { ?>
    <style type="text/css">
    	.no-svg body.login div#login h1 a{
	    	background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/img/c-title-logo3.png);
    	}
        body.login div#login h1 a {
            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/img/c-title-logo3.svg);
            padding-bottom: 20px;
            background-size: 82px 82px;
            width: 82px;
            margin-top: 30px;
        }
        body.login {background-color: #e1d8e6;}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'wp_foundation_login_logo' );

//for ie8 and under
function add_ie_html5_shim () {
    echo '<!--[if lt IE 8]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');


function webfonts_load_styles() {
  if (!is_admin()) {

    wp_register_style('googleFont', 'http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic|Raleway:300,400');

    wp_enqueue_style('webfonts', get_stylesheet_uri(), array('googleFont') );
  }
}
add_action('wp_enqueue_scripts', 'webfonts_load_styles');

function webfonts_load_styles_2() {
  if (!is_admin()) {
    wp_register_style('googleFont2', 'http://fonts.googleapis.com/css?family=Fjalla+One&text=CAULINORT');
    wp_enqueue_style('webfonts2', get_stylesheet_uri(), array('googleFont2') );
  }
}
add_action('wp_enqueue_scripts', 'webfonts_load_styles_2');





add_action( 'wp_head', 'wp_foundation_touch_icons'); 

function wp_foundation_touch_icons () { ?> 
<!-- non-retina iPhone pre iOS 7 -->
<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon-iOS6.png" sizes="57x57">
<!-- non-retina iPad pre iOS 7 -->
<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon-72.png" sizes="72x72">
<!-- non-retina iPad iOS 7 -->
<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon-76.png" sizes="76x76">
<!-- retina iPhone pre iOS 7 -->
<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon-iOS6@2x.png" sizes="114x114">
<!-- retina iPhone iOS 7 -->
<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon@2x.png" sizes="120x120">
<!-- retina iPad pre iOS 7 -->
<link rel="apple-touch-icon" href="/apple-touch-icon-72@2x.png" sizes="144x144">
<!-- retina iPad iOS 7 -->
<link rel="apple-touch-icon" href="/apple-touch-icon-76@2x.png" sizes="152x152">
<!-- Win8 tile -->
<meta name="msapplication-TileImage" content="favicon-144.png">
<meta name="msapplication-TileColor" content="#B20099"/>
<meta name="application-name" content="name" />
<?php }

/**
 * CV Upload on contact form
 */
 
function insert_attachment($file_handler,$post_id, $setthumb='false') {
 
  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();
 
  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');
 
  $attach_id = media_handle_upload( $file_handler, $post_id );
 
  if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);
  return $attach_id;
}

/**
 * For Page IDs to work correctly with WPML 
 */ 
function lang_page_id($id){
  if(function_exists('icl_object_id')) {
    return icl_object_id($id,'page',true);
  } else {
    return $id;
  }
}
function lang_post_id($id){
  if(function_exists('icl_object_id')) {
    return icl_object_id($id,'post',true);
  } else {
    return $id;
  }
  
}
function lang_category_id($id){
  if(function_exists('icl_object_id')) {
    return icl_object_id($id,'category',true);
  } else {
    return $id;
  }
  
}
function pp($value){
	echo '<pre>';
	print_r($value);
	echo '</pre>';
}
// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<div><a class="small button no-bottom-margin radius" href="'. get_permalink($post->ID) . '"> Read the full article...</a></div>';
}
add_filter('excerpt_more', 'new_excerpt_more');