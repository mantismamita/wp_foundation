jQuery(document).ready(function() {

	function calculateRoute(from, to) {
        // Center initialized to Naples, Italy
        var myOptions = {
          zoom: 10,
          center: new google.maps.LatLng(48.48, 2.20),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // Draw the map
        var mapObject = new google.maps.Map(document.getElementById("map"), myOptions);
 
        var directionsService = new google.maps.DirectionsService();
        var directionsRequest = {
          origin: from,
          destination: to,
          travelMode: google.maps.DirectionsTravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC
        };
        directionsService.route(
          directionsRequest,
          function(response, status)
          {
            if (status == google.maps.DirectionsStatus.OK)
            {
              new google.maps.DirectionsRenderer({
                map: mapObject,
                directions: response
              });
            }
            else
              jQuery("#error").append("Unable to retrieve your route<br />");
          }
        );
      }
      
    if (typeof navigator.geolocation == "undefined") {
          jQuery("#error").text("Your browser doesn't support the Geolocation API");
          return;
        }
 
        jQuery("#from-link, #to-link").click(function(event) {
          event.preventDefault();
          var addressId = this.id.substring(0, this.id.indexOf("-"));
 
          navigator.geolocation.getCurrentPosition(function(position) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
              "location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
            },
            function(results, status) {
              if (status == google.maps.GeocoderStatus.OK)
                jQuery("#" + addressId).val(results[0].formatted_address);
              else
                jQuery("#error").append("Unable to retrieve your address<br />");
            });
          },
          function(positionError){
            jQuery("#error").append("Error: " + positionError.message + "<br />");
          },
          {
            enableHighAccuracy: true,
            timeout: 10 * 1000 // 10 seconds
          });
        });
        
        jQuery("#from-hostel, #to-hostel").click(function(event) {
          event.preventDefault();
         
          var value="7 rue Aristide Bruant, 75018 Paris";
      
          jQuery(this).siblings("input").val("7 rue Aristide Bruant, 75018 Paris");
          console.log(jQuery(this));
          
        });
 
        jQuery("#calculate-route").submit(function(event) {
          event.preventDefault();
          calculateRoute(jQuery("#from").val(), jQuery("#to").val());
        });
	
	jQuery("#sorting dd:first-child").addClass('active');
	
	jQuery('#sorting dd').click(function() {
		// Remove the current active class
		// Add the active class to the clicked button
		jQuery('#sorting dd.active').removeClass('active');
		jQuery(this).addClass('active');
		
		// Get the button text (filter value)
		var filterValue = 'cat-' + jQuery(this).data('sort');
		console.log(filterValue);
				
		// If we clicked "All", we show all hidden items
		if (filterValue === 'cat-all') {
			jQuery('.room-entry').removeClass('hidden');
		} else {
			// Else, we find the portfolio entries that match 
			// and add the class of .hidden
			jQuery('.room-entry').each(function() {
				if (!jQuery(this).hasClass(filterValue)) {
					jQuery(this).addClass('hidden');
				} else {
					jQuery(this).removeClass('hidden');
				}
			});
		}
	
		return false;
	});
	
	jQuery('.cloneButton').click(function(event){
	
	event.preventDefault();
	
    var currentCount =  jQuery('.repeatingSection').length;
    var newCount = currentCount+1;
    var lastRepeatingGroup = jQuery('.repeatingSection').last();
    var newSection = lastRepeatingGroup.clone(false).find('.cloneButton').remove().end();
    
    newSection.insertAfter(lastRepeatingGroup);
    newSection.find("input").each(function (index, input) {
        input.id = input.id.replace("_" + currentCount, "_" + newCount);
        input.name = input.name.replace("_" + currentCount, "_" + newCount);
    });
    newSection.find("label").each(function (index, label) {
        var l = jQuery(label);
        l.attr('for', l.attr('for').replace("_" + currentCount, "_" + newCount));
    });
    return false;
	});
	
	jQuery(document.body).on('click','.removeButton', function(){
	
		jQuery(this).closest('div.repeatingSection').remove();
		return false;
	});
	
(function ($) {
    var now = new Date();
    var date = $('.form-date');
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);

    date.val(today);

    var form = $('form');

    form.on('submit', function () {
        var currentForm = $(this);
        var newVal = currentForm.find('.form-date').val().split('-'),

            dateParts = {
                year: parseInt(newVal[0], 10),
                month: parseInt(newVal[1], 10),
                day: parseInt(newVal[2], 10)
            };

        currentForm.find('.anchor').append($.map(dateParts, function (index, key) {
            console.log(['date', form.index(currentForm), key].join('_'));
            return $('<input>', {
                type: 'hidden',
                name: ['date', key].join('_'),
                id: ['date', form.index(currentForm), key].join('_'),
                value: dateParts[key]
            });
        }));
        
    });
})(jQuery);	


	(function() {  
      var elem = document.createElement('input');  
      elem.setAttribute('type', 'date');  
  
      if ( elem.type === 'text' ) {  
         jQuery('.datepicker').datepicker({
    
	    dateFormat: 'dd/mm/yy',
	    firstDay: 1,
	  
		});
      }  
   })(); 
	
}); 


