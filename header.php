<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package wp_foundation
 */
?><!DOCTYPE html>

<!--[if lt IE 9]>      <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> > <!--<![endif]-->
  <head>
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" >
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />


  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- for screenreaders etc. -->
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
</div>
	</header>
</div>
<!-- end for screenreaders etc. -->

    <!-- Navigation -->

  <nav id="site-navigation" class="main-navigation top-bar" role="navigation"data-topbar data-options="custom_back_text: false">
  <div class="skip-link"><a class="screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wp_foundation' ); ?></a></div>
  
  
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        <h1>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"></a>
        </h1>
      </li>
      <li class="toggle-topbar menu-icon"><a href="#"><span><?php _e( 'Menu', 'wp_foundation' ); ?></span></a></li>
    </ul>
 
     <section class="top-bar-section">
    
            	<?php 
            	
            	$args = array(
					'theme_location'  => 'primary',
					'container'       => 'section',
					'container_class' => 'top-bar-section',
					'menu_class'      => 'right',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu'
				);
				
				wp_nav_menu( $args );?>
 
        </section>
  </nav>


  <!-- End Top Bar -->